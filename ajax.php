<?php

 header('Access-Control-Allow-Origin: *'); 

/*
* This file will return the lements between two indexs
*/

$lorem = ("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod".
				"tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,".
				"quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo".
				"consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse".
				"cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non".
				"proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

function randomInt(){
	return rand(1,256);
}

function stringGen ($len = 4){
    $text = "";
    $charset = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    for( $i=0; $i < $len; $i++ ){
        $text .= $charset[ rand(0, strlen($charset)) ];
    }
    
    return $text;
}

$indexStart = (int)(isset($_GET['from']) ? $_GET['from'] : 0);
$indexEnd = (int)(isset($_GET['to']) ? $_GET['to'] : 0);

$items = array();


for ($i=0; $i < 20 ; $i++) { 
	
	$items[] = array(
			"title" => stringGen(),
			"descriptionShort" => $lorem,
			"id" => ($i+1),
			"author" => [
				"pseudo" => "ASSANI Zakaria",
				"postedArticles" => randomInt()
			]
		);
}


exit(json_encode(array_slice($items, $indexStart, ($indexEnd-$indexStart) )));

