

/*
* Contain array datas for dev.
*/


var articles = [],

stringGen = function (len = 4){
    var text = "",
    charset = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    for( var i=0; i < len; i++ )
        text += charset.charAt(Math.floor(Math.random() * charset.length));
    
    return text;
},

getRandomInt = function (min = 0, max = 100) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
},

lorem = ("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"+
				"tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,"+
				"quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo"+
				"consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse"+
				"cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non"+
				"proident, sunt in culpa qui officia deserunt mollit anim id est laborum.") ,


initArticles = function(nb = 115){

	for (var i = 0; i < nb; i++) {
		var article = {
			title: stringGen(),
			descriptionShort: lorem,
			id: (i+1),
			author: {
				pseudo: "ASSANI Zakaria",
				postedArticles: getRandomInt()
			}
		};

		articles.push(article);
	}
};

initArticles(20); 