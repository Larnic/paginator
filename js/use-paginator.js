
/*
* Simple Paginator
*/

(function(){

	'use strict';

	var spaginator = Paginator.createPaginator({ 
		type: "Simple",
		itemsProvided: { state: true, items: articles },
		itemsPerPage: 5,
		containerSelector: "#elementsContainerS",
		displayedPages: 2,
		onPageClick: function(){},
		addLineFunction: function(element){
			return ('<div class="element" id="'+element.id+'">'+
			'<div class="title align-center"> <p>'+element.title+'</p></div>'+
			'<div class="content">'+
				'<p>'+element.descriptionShort+'</p>'+
			'</div>'+
			'<div class="author align-center">'+
				'<p>By '+element.author.pseudo+' ('+element.author.postedArticles+')</p>'+
					'</div>'+
				'</div>'+
			'</div>');	
		},
		theme: 'light'
	});

})();

(function(){

	'use strict';

	var apaginator = Paginator.createPaginator({ 
		type: "AJAX",
		itemsProvided: { state: false, items: [] },
		itemsPerPage: 5,
		totalItems: 25,
		url: "http://www.localhost/paginator/ajax.php",
		indexNames: {indexStart: "from", indexEnd: 'to'},
		indexIsCurrentPage: { state: false, name: 'p' },
		displayedPages: 2,
		containerSelector: "#elementsContainerA",
		onPageClick: function(){
			//Add loader
			$("#elementsContainerA").prepend("<div class='cover'> <div class='loader'></div></div>");
		},
		addLineFunction: function(element){
			return ('<div class="element" id="'+element.id+'">'+
			'<div class="title align-center"> <p>'+element.title+'</p></div>'+
			'<div class="content">'+
				'<p>'+element.descriptionShort+'</p>'+
			'</div>'+
			'<div class="author align-center">'+
				'<p>By '+element.author.pseudo+' ('+element.author.postedArticles+')</p>'+
					'</div>'+
				'</div>'+
			'</div>');	
		},
		theme: 'dark'
	});
})();