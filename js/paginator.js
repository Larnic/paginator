
/*
 * JS Paginator
 * Copyright(c) 2017 ASSANI Zakaria - z.madi293@gmail.com
 * MIT Licensed
 */

 var Paginator = { },
 Theme = {};

(function(){

	'use strict';

	/*
	* The paginator will build and create complete pagination 
	* for provide elements. 
	*/


	/*
	* Contain list of difines themes
	*/
	var themes = ["light", "dark"];

	/*
	* Add theme
	*/
	Theme.addTheme = function(theme){
		var state = 1;
		if(theme != "" && theme != undefined && theme.length > 3 && themes.indexOf(theme) < 0)
			themes.push(theme);
		else if (themes.indexOf(theme) >= 0)
			state = 2;
		else
			state = 0;


		return state;
	}

	/*
	* get Thmes
	*/
	Theme.getThemes = function(){
		return themes;
	}

	/*
	* Apply the the on pagination
	*/
	Theme.applyTheme = function(theme, paginatorObject){
		var state = 0;

		if(theme != "" && theme != undefined && theme.length > 3 && themes.indexOf(theme) >= 0){
			$.each(themes, function(index, eTheme){
				$(paginatorObject.getContainerSelector()+" + "+ paginatorObject.getPaginationSelector()+" ul ").removeClass(eTheme);
			});

			$( paginatorObject.getContainerSelector() +" + "+ paginatorObject.getPaginationSelector()+" ul ").addClass(theme);
			state = 1;
		}

		return state;
	}


	/*
	* Parameters : Object
	*
	* type : Simple or AJAX
	* itemsProvided : Object
	* buildArrayFromHTML : Object
	* nbElementsPerPage : Elements per page (int)
	* containerSelector : CSS or JQquery selector for the elements container (string)
	* addLineFunction : Call back function to add elements on container (function)
	* 
	*/

Paginator.createPaginator = function(CONFIGS){

	var $this = { };


		/*
		* Source the index in configs Object
		*/
		$this.getPropertyOnConfigs = function(element){
			if(CONFIGS.hasOwnProperty(element))
				return CONFIGS[element];
			else
				return null;
		};

		/*
		* Generate Uniq id
		*/
		$this.guid = function(prefix) {

			if(prefix == undefined){
				var prefix = true;
			}

		  function s4() {
		    return Math.floor((1 + Math.random()) * 0x10000)
		      .toString(16)
		      .substring(1);
		  }
		  var prefix = (prefix == true ? "elements-pagination-" : "" );
		  return prefix+s4() + s4() +
		    s4() + '-' + s4() + s4() + s4();
		};

		/*
		* Check if object is correcty intanciated with params
		*/

		switch($this.getPropertyOnConfigs("type")){
			case "Simple":
					if(CONFIGS == undefined || Object.keys(CONFIGS).length < 4){
						/*
						* Defaults parameters
						*/
						var CONFIGS = {
							type: "Simple",
							itemsProvided: { state: true, items : [] },
							itemsPerPage: 20,
							containerSelector: "",
							paginationContainer: "",
							addLineFunction: function(){},
							displayedPages: 1,
							prevText: "Prev",
							nextText: "Next",
							onPageClick: function(){},
							theme: "light"
						}
					}
				break;
			case "AJAX":
				if(CONFIGS == undefined || Object.keys(CONFIGS).length < 4){
					var CONFIGS = {
						type: "AJAX",
						itemsProvided: { state: false, items: [] },
						itemsPerPage: 20,
						totalItems: 0,
						url: "",
						paginationContainer: "",
						indexNames: {indexStart: "from", indexEnd: "to"},
						indexIsCurrentPage: { state: true, name: 'p' },
						containerSelector: "",
						addLineFunction: function(){},
						displayedPages: 1,
						prevText: "Prev",
						nextText: "Next",
						onPageClick: function(){},
						theme: "light"
					}
				}
				break;
		}

		/*
        * Return all pages
        */
        $this.getNbPage = function(){
        	return ( instance.getPropertyOnConfigs('type') == "AJAX" ? Math.ceil( (instance.getPropertyOnConfigs('totalItems') / nbElementsPerPage) ) : Math.ceil( (showEmpList.length / nbElementsPerPage) ) );
        };


		/*
		* List of elements to show
		*/
		var showEmpList = ($this.getPropertyOnConfigs('itemsProvided').state == true ?  $this.getPropertyOnConfigs('itemsProvided').items : [] ),

		/*
		* Call back on page click
		*/
		onPageClick = ($this.getPropertyOnConfigs('onPageClick') != null && typeof($this.getPropertyOnConfigs('onPageClick')) == "function" ?  $this.getPropertyOnConfigs('onPageClick') : function(){} ),

		/*
		* Theme to use
		*/
		theme = ($this.getPropertyOnConfigs('theme') != null && themes.indexOf($this.getPropertyOnConfigs('theme')) > -1  ?  $this.getPropertyOnConfigs('theme') : 'light' ),

		/*
		* Current page
		*/
		currentPage = 1, 

		/*
		* Elements par page
		*/
		nbElementsPerPage = $this.getPropertyOnConfigs("itemsPerPage"),

		/*
		* Enregistre la fonction d'ajout d'un élément
		*/
		addOneLine = $this.getPropertyOnConfigs("addLineFunction"),
		/*
		* Add elements to show in the container
		*/
		addLinesFunction = function(){

			$(instance.getContainerSelector()).empty();
			$.each(instance.getShowElements(), function(index, value){
					$(instance.getContainerSelector()).append(
						addOneLine(value)
					);
			});
			
		},

		/*
		* Elements containern [ HTML ]
		*/
		containerSelector = $this.getPropertyOnConfigs("containerSelector"),

		/*
		* Current paginator Instance / for JQuery context 
		*/
		instance = $this,

		/*
		* Pagination id
		*/
		paginationId = $this.guid(), 

		/*
		* conteneur de la pagination
		*/
		paginatorContainerSelector = ($this.getPropertyOnConfigs('paginationContainer') != null ? $this.getPropertyOnConfigs('paginationContainer') : "#"+paginationId),

		/*
		* JQuery checker 
		*/
		JQueryIsLoad = function(){
			return ( typeof jQuery == 'undefined' ? false : true );
		},

		/*
		* Stop Script 
		*/
		stopScript = function(message){

			var message = (message == undefined ? "Script stopped by an fatal error." : message);
			throw new Error(message);
		},

		/*
		* Récupère les élémnets via AJAX en asynchrone.
		*/
		getElementsByAJAX = function(indexStart, indexEnd){

			var asynchronous = (arguments[2] == undefined ? true : true ),
			$name = instance.getPropertyOnConfigs('indexIsCurrentPage').name,
			datas = { };
			if(instance.getPropertyOnConfigs('indexIsCurrentPage').state)
			 datas[ $name ] = currentPage ;
			 else
			datas = { from: indexStart, to: indexEnd } ;

			$.ajax({type: 'GET',
	            url: instance.getPropertyOnConfigs('url'),
	            data: datas,
	            dataType: 'json',
	            async: asynchronous,
	            success: function (data) {

	            	if(data.length > 0){
		            	$.each(data, function(index, element){
		            		showEmpList.push(element);
		            	});
		            	addLinesFunction();

		            	if($(paginatorContainerSelector + " ul > li").length == 1)
		            		$(paginatorContainerSelector + " ul > li").data({ hasCache: true });
		            	else
		            		$(paginatorContainerSelector + " ul > li:eq("+currentPage+")").data({ hasCache: true });

		            }else{
		            	stopScript("Nothing get from the server !");
		            }
	            },
	            error: function (response) {
	                stopScript("The AJAX query fail, check your url and your params");
	            }
	        });
		},

		/*
		* Add the paginator container after elmenets container
		*/
		addPaginatorContainer = function(){

				if(paginatorContainerSelector  == "#"+paginationId){
					$(containerSelector).after('<div id="'+paginationId+'">'+
		                    '<ul>'+
		                        '<!-- Pagination elements -->'+
		                    '</ul>'+
		            '</div>');
				}else{
					$(paginatorContainerSelector).append('<div id="'+paginatorContainerSelector.replace("#", "")+'">'+
		                    '<ul>'+
		                        '<!-- Pagination elements -->'+
		                    '</ul>'+
		            '</div>');
				}

			  if(instance.getPropertyOnConfigs('type') == "AJAX")
				getElementsByAJAX(0, nbElementsPerPage);

				regeneratePagination(true);
				$(paginatorContainerSelector + " ul > li:eq("+currentPage+")").data({ hasCache: true });
				addLinesFunction();
			
		},

		/*
        * Regenerate the pagination 
        *  with Elements array length and nbElementsPerPage 
        */
        regeneratePagination = function(initialisation){

        	if(initialisation == undefined){
        		initialisation = true;
        	}
        	
        	var nbPage = instance.getNbPage(),
        		next = "<li class='next'><p>"+(instance.getPropertyOnConfigs("nextText") != null ? instance.getPropertyOnConfigs("nextText") : "Next")+"</p></li>",
        		prev = "<li class='prev disabled'><p>"+(instance.getPropertyOnConfigs("prevText") != null ? instance.getPropertyOnConfigs("prevText") : "Prev")+"</p></li>";
        	$(paginatorContainerSelector + " ul").empty();

        	if(nbPage > 1)
        		$( paginatorContainerSelector + " ul").append(prev);

        	var displayedPages = ( instance.getPropertyOnConfigs('displayedPages') == null ? instance.getNbPage() :  (instance.getPropertyOnConfigs('displayedPages') < 1 ? 1 : instance.getPropertyOnConfigs('displayedPages') ) );

        	for (var i = 0; i <= displayedPages; i++) {
        		
        		if( i < displayedPages && i < instance.getNbPage()){
	        		//add item to list
	        		if(instance.getPropertyOnConfigs('type') == "AJAX")
	        			$( paginatorContainerSelector + " ul").append("<li>"+(i+1)+"</li>").data({ hasCache: false });
	        		else
	        			$( paginatorContainerSelector + " ul").append("<li>"+(i+1)+"</li>");
        			
        		}
        		
        	}

        	for (var i = displayedPages; i < instance.getNbPage(); i++) {      			


        			//add hide item to list
	        		if(instance.getPropertyOnConfigs('type') == "AJAX")
	        			$( paginatorContainerSelector + " ul").append("<li class='hide'>"+(i+1)+"</li>").data({ hasCache: false });
	        		else
	        			$( paginatorContainerSelector + " ul").append("<li class='hide'>"+(i+1)+"</li>");
        		
        	}

        	if(nbPage > 1)
        		$( paginatorContainerSelector + " ul").append(next);

        	if(nbPage > 1)
        	 	$( paginatorContainerSelector + " ul li").eq(1).addClass('active');
        	 else
        	 	$( paginatorContainerSelector + " ul li").eq(0).addClass('active');
        },

        /*
        * Réorganise la pagination en fonction des paramètres
        */
        reorder = function(){
        	var displayedPages = ($this.getPropertyOnConfigs('displayedPages') == null ? 1 : $this.getPropertyOnConfigs('displayedPages'));

        	if(displayedPages > 1){

        		var currentPageItem = $( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").eq(currentPage-1);

        		//si on a pas un nombre équivalent de chaque côté


        		if(displayedPages <= 0)
        			displayedPages = 1;

        		if(displayedPages%3 == 0 ){
        			displayedPages = displayedPages-1;
        		}

        		if(currentPage > (displayedPages/2) && currentPage <= (instance.getNbPage()+1 - (displayedPages/2)) ){
        		
        		//Affichage des éléments
        		$( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").removeClass("active").addClass('hide').eq(currentPage-1).removeClass('hide').addClass('active');

        		var prevElements = $( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").eq(currentPage-1).prevAll("li:not(li.next, li.prev)"),
        			nextElements = $( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").eq(currentPage-1).nextAll("li:not(li.next, li.prev)");

        			prevElements = prevElements.toArray().reverse();
        			nextElements = nextElements.toArray();

        			for (var i = 0; i < (displayedPages/2); i++) {

        				if(displayedPages/2 == 1){
        					$(prevElements).eq( prevElements.length-1 ).removeClass('hide');
        				}
        				else{
        					$(prevElements).eq( (prevElements.length)-i ).removeClass('hide');
        				}
        				
        				$(nextElements).eq( i ).removeClass('hide');
        			}
        		}
        		

        	}else{
        		$( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").removeClass("active").addClass('hide').eq(currentPage-1).removeClass('hide').addClass('active');
        	}
        };

        /*
        * Return elements to show
        * according to the current page.
        */
        $this.getShowElements = function(){
        	var indexMin = ((currentPage*nbElementsPerPage)-nbElementsPerPage),
        	indexMax = (currentPage*nbElementsPerPage)-1;
        	
        	if(showEmpList[ (currentPage*nbElementsPerPage)-nbElementsPerPage] == undefined && showEmpList[ (currentPage*nbElementsPerPage)-1 ] == undefined )
        		return showEmpList.slice( (showEmpList.length-nbElementsPerPage-1) , showEmpList.length-1 ); 
        	else
        		return showEmpList.slice((indexMin >= 0 ? indexMin : 0), indexMax+1);
        };

        /*
        * Si JQuery n'est pas chergé on arrête le script
        */
		if(!JQueryIsLoad())
			stopScript("JQuery is not Load, are you correctly include JQuery ? ");
			
        /*
		* S'occupe de mettre à jour les index pour afficher
		* les bons éléments
		*/
		$(document).on("click", paginatorContainerSelector + " ul > li:not(li.next, li.prev)", function(event){

				instance.setPage(parseInt($(this).text()));
				$( paginatorContainerSelector + " ul > li").removeClass('active');
				$(this).addClass('active');
				
				onPageClick();

				if(instance.getPropertyOnConfigs('type') == "AJAX"){
					if(!$(this).data().hasCache)
						getElementsByAJAX( (currentPage-1)*nbElementsPerPage, (currentPage)*nbElementsPerPage );
					else
						addLinesFunction();
				}
				else{
					addLinesFunction();
				}

				$("#load_elements").addClass("hide");

				if(currentPage == 1)
					$( paginatorContainerSelector + " ul > li.prev").addClass('disabled');
				else
					$( paginatorContainerSelector + " ul > li.prev").removeClass('disabled');

				if(currentPage == instance.getNbPage()+1)
					$( paginatorContainerSelector + " ul > li.next").addClass('disabled');
				else
					$( paginatorContainerSelector + " ul > li.next").removeClass('disabled');

		});

		/*
		* S'occupe de mettre à jour les index pour afficher
		* les bons éléments sur next et prev
		*/
		$(document).on("click", paginatorContainerSelector  + " ul > li.next, "+ paginatorContainerSelector + " ul > li.prev", function(event){

					var page = currentPage;
					switch($(this).hasClass('next')){ 
						case true:
							if( currentPage < instance.getNbPage()+1 && currentPage >= 1){
								page = (currentPage+1);
								$(this).removeClass('disabled');
								$( paginatorContainerSelector + " ul > li.prev").removeClass('disabled');
								
								$( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").eq(currentPage).trigger('click');
								reorder();
								instance.setPage(page);
							}
							else{
								$(this).addClass("disabled");
							}

							
							break;
						case false:
							if(currentPage <= instance.getNbPage()+1 && currentPage > 1 ){
								page = (currentPage-1);
								$(this).removeClass('disabled');
								$( paginatorContainerSelector + " ul > li.next").removeClass('disabled');
								
								$( paginatorContainerSelector + " ul > li:not(li.next, li.prev)").eq(currentPage-2).trigger('click');
								reorder();
								instance.setPage(page);
							}
							else{
								$(this).addClass('disabled');
							}


							break;
					}


		});

		/*
		* For simple-paginator Build array
		* hide elements on HTML
		*/
		$.each($(containerSelector).children(), function(index, element){
			if(index < nbElementsPerPage)
				$(element).hide();
		});

		/*
		* Setter functions
		*/
		$this.setPage = function(page){
			currentPage = (page == undefined ? 0 : page);
		}

		/*
		* Return elements per page
		*/
		$this.getNbElementsPerPage = function(){
			return nbElementsPerPage;
		}

		/*
		* Return the container selector
		*/
		$this.getContainerSelector = function(){
			return containerSelector;
		}

		/*
		* Get current show elements
		*/
		$this.getShowEmpList = function(){
			return showEmpList;
		}

		/*
		* Get pagination selector
		*/
		$this.getPaginationSelector = function(){
			return paginatorContainerSelector;
		}

		/*
		* Init paginator with self function calls
		*/
		addPaginatorContainer();

		/*
		* Set the correct theme
		*/
		$( paginatorContainerSelector+ " ul").addClass(theme);

		return $this;
}

})();